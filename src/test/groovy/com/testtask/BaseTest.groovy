package com.testtask

import com.jayway.restassured.RestAssured
import com.jayway.restassured.authentication.AuthenticationScheme
import com.testtask.model.TwitterUser
import com.testtask.utils.TestPropertiesLoader
import org.junit.BeforeClass

import static com.jayway.restassured.RestAssured.oauth

class BaseTest {

    public static final TwitterUser INVALID_USER = new TwitterUser(0L, "TWITTER_TEST_INVALID_USER")

    @BeforeClass
    static void setupClass() {
        RestAssured.baseURI = setupTwitterBaseUri()
        RestAssured.authentication = setupOauthAuthentication()
        RestAssured.enableLoggingOfRequestAndResponseIfValidationFails()
    }

    private static String setupTwitterBaseUri() {
        TestPropertiesLoader.getTestProperties().baseUrl + "/" + TestPropertiesLoader.getTestProperties().apiVersion
    }

    private static AuthenticationScheme setupOauthAuthentication() {
        oauth(
            TestPropertiesLoader.getTestProperties().consumerKey as String,
            TestPropertiesLoader.getTestProperties().consumerSecret as String,
            TestPropertiesLoader.getTestProperties().accessToken as String,
            TestPropertiesLoader.getTestProperties().accessSecret as String
        )
    }
}

package com.testtask.endpoints

import com.jayway.restassured.RestAssured
import com.testtask.BaseTest
import com.testtask.model.ServiceErrorDescription
import org.hamcrest.CoreMatchers
import org.junit.AfterClass
import org.junit.BeforeClass
import org.junit.Test

import static com.jayway.restassured.RestAssured.given
import static org.hamcrest.CoreMatchers.hasItem

//https://dev.twitter.com/rest/reference/get/statuses/user_timeline
class UserTimelineTest extends BaseTest {

    @BeforeClass
    static void setup() {
        RestAssured.basePath = "/statuses/user_timeline.json"
    }

    @AfterClass
    static void cleanup() {
        RestAssured.basePath = null
    }

    @Test
    void "when no query params are specified, then return response with code 200 and non-empty body"() {
        given()
        .when()
            .get()
        .then()
            .assertThat()
                .statusCode(200)
                .body(CoreMatchers.notNullValue())
    }

    @Test
    void "when user id is invalid, then return response with code 404 and error description"() {
        given()
            .param("user_id", INVALID_USER.id)
        .when()
            .get()
        .then()
            .assertThat()
                .statusCode(404)
                .body("errors.code", hasItem(ServiceErrorDescription.PAGE_DOES_NOT_EXIST.code)).and()
                .body("errors.message", hasItem(ServiceErrorDescription.PAGE_DOES_NOT_EXIST.message))
    }
}

package com.testtask.model


enum ServiceErrorDescription {
    PAGE_DOES_NOT_EXIST(34, "Sorry, that page does not exist.")

    int code
    String message

    ServiceErrorDescription(int code, String message) {
        this.code = code
        this.message = message
    }
}

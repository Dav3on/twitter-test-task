package com.testtask.model

import groovy.transform.Canonical


@Canonical
class TwitterUser {

    long id
    String name
}

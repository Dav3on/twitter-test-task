package com.testtask.utils

class TestPropertiesLoader {

    private static final String FILE_NAME = "application.properties"

    private static Properties properties

    static Properties getTestProperties() {
        if (properties == null) {
            InputStream inputStream = TestPropertiesLoader.class.getClassLoader().getResourceAsStream(FILE_NAME)
            try {
                properties = new Properties()
                properties.load(inputStream)
            } catch (IOException e) {
                throw new IllegalStateException("Failed to read test properties: " + FILE_NAME, e)
            } finally {
                try {
                    inputStream.close()
                } catch (IOException ignored) {
                }
            }
        }
        properties
    }
}
